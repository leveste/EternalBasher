# Auto Heckin' Texture Converter

Automatically converts any texture file(s) to the TGA format required by the game, eliminating the need of using Nvidia's tool to convert to BC1a and then DivinityMashine manually.

Requires to have the Nvidia Texture Tools installed, you can compile and install them from [here](https://github.com/PowerBall253/nvidia-texture-tools). Also requires dotnet 5.0 for DivinityMashine to work.

## Instructions:

* Download this script and the 'tools' folder.

* Make the file executable with:
```
chmod +x "Auto Heckin' Texture Converter.sh"
```

* Run the file with the following syntax:
```
./"Auto Heckin' Texture Converter.sh" file1 file2 [...]
```
